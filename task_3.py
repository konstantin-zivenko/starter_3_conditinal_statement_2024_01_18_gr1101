# На вхід дається будь-яке число. Дізнатися скільки розрядів у числі- воно (1, 2, 3 або більш значне), позитивне чи
# негативне? Наприклад, 25 - це двозначне позитивне, а -345 це тризначне негативне, а 2400 - це 3х і більш
# значне позитивне. 0 враховувати як окремий варіант, а не як однозначне число.


num_str = input("input number: ")

num_int = int(num_str)
sign = "positive" if num_int > 0 else "negative"
if abs(num_int) == 0:
    print("equal null")
elif abs(num_int) // 10 == 0:
    print(sign, "one digit")
elif abs(num_int) // 100 == 0:
    print(sign, "two digit")
else:
    print(sign, "three or more digit")
