# Написати програму, яка визначає, що звір перебуває перед нами. Якщо звір каже “Meow”, це кішка, якщо “Bark”,
# це собака, і якщо щось інше, це “Невідомий звір”.

sound = input("animals word: ")

if sound == "Meow":
    print("cat")
elif sound == "Bark":
    print("dog")
else:
    print("unknown animal")
